# Liste d'outils et de plateformes Libre et open source pour créer, gérer des applications mobile et web en nocode, lowcode

Juste une liste d'applications libre et open source de nocode, lowcode | Just a Free/Libre Open Source Software nocode-Lowcode application list.

> _Merci de garder en mémoire la cible : Des outils et plateformes du monde du **logiciel libre et open source** pour réaliser des applications web et mobile en **nocode et lowcode**._

> _Please keep in mind the target: **free and open source software** tools and platforms to develop web and mobile applications in **nocode and lowcode**._

# La liste | The list

## Orienté prototype : croquis, Wireframing, mockup

- Pencil Projet (https://pencil.evolus.vn)
- Penpot https://penpot.app/) (Ex-OpenBot)
- Figma-Linux (https://github.com/Figma-Linux/figma-linux) que je ne connais pas.
        Se présente : Figma-linux is an unofficial Electron-based Figma desktop app for Linux.
- vos propositins (FLOSS), …

## Développements d’applications mobiles et/ou web

- MIT App Inventor 2 http://www.appinventor.mit.edu/
- Thunkable (automatisation basée sur AppInventor) https://thunkable.com/ Build powerful, native mobile apps without coding.
- Silex (https://www.silex.me/)
- Convertigo (Low-Code / No-Code , Open Source Web & mobile application development platform - Convertigo)
- Infinable (https://infinable.io/)
- Saltcorn (https://github.com/saltcorn/saltcorn)
- Android Studio (Download Android Studio and SDK tools  |  Android Developers) Open source et produit Google
- Supabase (https://supabase.io/) *** alternative OS à Firebase
- Tooljet https://tooljet.com/ Open-source low-code framework to build & deploy internal tools within minutes. @fox
- BudiBase https://budibase.com/ Create business apps in minutes @fox
- vos propositins (FLOSS), …

## Applications for mobile and web apps : Base de données :

- NocoDB : (https://www.nocodb.com/)
- Baserow (https://baserow.io/) *** Merci à @Nathaniel
- directus (https://directus.io/) " An Instant App & API for your SQL Database." *** merci @tcit
- BaseTool https://www.basetool.io/ View and manage all your data in one place like a pro @fox 
- vos propositins (FLOSS), …

## Applications for mobile and web apps : API/workflow :

- n8n (https://n8n.io/)
- Beehive (https://github.com/muesli/beehive)
- DataFire (framework) https://github.com/DataFire/DataFire
- Huginn (https://github.com/huginn/huginn)
- vos propositins (FLOSS), …

## À étudier / confirmer :) | To be validated

- LocoKit  https://locokit.io/ https://github.com/locokit/locokit The Low Code Kit platform. 
        - (Erreur de lien sur la première référence. Le lien vers LocoKit est désormais à jour)

- HASURA a été signalé. https://hasura.io/ Instant GraphQL on all your data. Je ne suis pas sûr qu'il entre dans cette liste des applications nocode / Lowcode :).

## Commentaires additionnels | Some additional Commentaires

- Draftbit (https://draftbit.com) a été un temps dans cette liste. Draftbit est propriétaire et il est mentionné " that code is based on open-source frameworks and libraries". En fait, il est basé sur https://expo.dev/, un framework pour le développement d'applications en React. Expo, n'est donc pas une Nocode / Lowcode application. Bien que FLOOS, il ne figure donc pas ici :).
        Je ne suis pas sûr de bien voir les choses. Drafit semble « propriétaire ». Et il s’appuie sur un Framework open source. 

- SeaTable est « libre » d’usage, mais pas dans le code source. *** Merci @fox. Je l'ai retiré de la liste (jrd10). Je laisse le commentaire ci-dessous.
        - SeaTable (SeaTable - simple comme Excel, puissant comme une base de données) « Il ressemble à Excel, mais a tellement plus à offrir. » *** merci @tcit

- L’espace Community de Figma https://www.figma.com/community (Figma est propriétaire). Son espace Communauté permet la création de projets libres et open source).

# Quelques références | Some references

1. https://nocode-france.fr (focalise sur le nocode/low code, pas sur le FLOSS)
2. Rediffusion d'une vidéoconférence : `Panorama des outils nocode / low-code, alternatives à AirTable` https://www.youtube.com/watch?v=7erMuOHWhnk de Mathieu Dartigues (@fox)


## Origine et contributions | Source and contributions

Cette page a été initiée suite à un fil sur Framacolibri : https://framacolibri.org/t/le-nocode-libre-et-open-source-pour-apps-mobile-et-web/13260/16

This page has created following this thread (in French): https://framacolibri.org/t/le-nocode-libre-et-open-source-pour-apps-mobile-et-web/13260/16

N'hésitez pas à proposer vos références | Fell free to propose your references

## Licence
Dans le domaine public CC0 1.0 | CC0 1.0 Public Domain

## Situation du projet | Project status
C'est un projet ouvert qui peut être rejoint par toute personne intéressées sans prosélytisme excessif :) | This project is opten to all person interessed by the topic, without undue proselytizing :).


### Déc 2021.

> Si vous souhaitez excercer un droit de retrait ou de correction sur un ou des éléments dont vous êtes propriétaire ou dont vous exercez une certaine expertise, n'hésiter à me contacter à travers cette plateforme.

> If you wish to exercise a right of withdrawal or correction on one or several elements of which you are the owner or of which you exercise a certain expertise, do not hesitate to contact me through this platform.

- Traduction assistée avec https://www.deepl.com | Assisted translation with https://www.deepl.com
